import {Builder, until} from 'selenium-webdriver'

async function runTest() {
    const driver = await new Builder().forBrowser('firefox').build();

    try {
        // Navigate to the website URL
        await driver.get('http://localhost:5173');

        // Wait until the page title contains the expected text
        await driver.wait(until.titleContains('weather app'), 3000);

        // Get the page title
        const pageTitle = await driver.getTitle();

        // Print the page title to the console
        console.log('Page title:', pageTitle);

        // Assert that the page title matches the expected value
        if (pageTitle === 'weather app') {
            console.log('Website name is correct!');
        } else {
            console.error('Website name is incorrect!');
        }
    } catch (error) {
        console.error('An error occurred:', error);
    } finally {
        await driver.quit();
    }
}

runTest();
