import History from '../models/history.js';

const postHistory = async (req) => {
    const temperature = req.currentConditions.temp
    const address = req.resolvedAddress
    const history = new History({ temperature, address });

    try {
        await history.save();
        console.log('History data saved successfully');
    } catch (err) {
        console.error(err)
    }
};

/*
const getHistory = async (req, res) => {
    const selectedHistory = req.body
    console.log(selectedHistory)
    try {
        const history = await History.find();
        res.json(history);
    } catch (err) {
        console.error('Error retrieving history data:', err);
        res.status(500).send('Internal Server Error');
    }
};
*/

export default postHistory