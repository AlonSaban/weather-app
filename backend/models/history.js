import mongoose from 'mongoose'

const HistorySchema = new mongoose.Schema({
    temperature: { type: String, required: true },
    address: { type: String, required: true }
}, { collection: 'history' });

const History = mongoose.model('History', HistorySchema);

export default History;
